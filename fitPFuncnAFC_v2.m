function [ fitobject, gof, ci, threshOut ] = fitPFuncnAFC_v2( level, response, nAFC, graphData,acuityOrContrast,threshLevel)

% Fit data from nAFC using cumulative gaussian psychometric function.
% Function returns fitted parameters, goodness of fit, and 95%
% confidence intervals for the fitted parameters.

% this version is adapted for the visualFieldTest script; function is fit
% with a negative slope for acuity threshold (spatial frequency on x-axis)
% and a positive slope for contrast threshold (contrast on the x-axis),
% based on acuityOrContrast variable
%
% also calculates the x-value corresponding to a performance level
% specified in threshLevel (e.g., 80% threshold), output in threshOut

if nAFC>0; pGuess=1/nAFC; % guess rate
else pGuess=0;
end

%bin data & calculate weights
arrangedData=arrangeData(single(level), response);
tLevel=arrangedData(:,1);
pCorrect=arrangedData(:,4);
dataErrEst=arrangedData(:,8);

%   nAFC cumulative gaussian
if acuityOrContrast == 1 %fit cumulative gaussian function with negative slope for acuity
    ft = fittype( @(thresh, slope,x)(1 + (1-pGuess).*-(0.5+0.5.*erf((x-thresh)./(sqrt(2).*slope)))));
else %positive slope for contrast
    ft = fittype( @(thresh, slope,x)(pGuess + (1-pGuess).*(0.5+0.5.*erf((x-thresh)./(sqrt(2).*slope)))));
end

% in case start point is far away from actual threshold, start point is based on last half of the trials:
secondHalfLevels = level(round(length(level)/2):end);
if std(secondHalfLevels) == 0
    stdGuess = std(tLevel);
else
    stdGuess = std(secondHalfLevels);
end
[fitobject, gof] = fit(tLevel, pCorrect,ft, 'StartPoint', [mean(secondHalfLevels) stdGuess], 'Weights', 1./dataErrEst);

%calculate the threshold estimate at threshLevel
if acuityOrContrast == 1
    threshOut = (erfinv((((1-threshLevel)/(1-pGuess))-.5)/.5)*(sqrt(2).*fitobject.slope)+fitobject.thresh); %for negative slope
else
    threshOut = (erfinv((((threshLevel-pGuess)/(1-pGuess))-.5)/.5)*(sqrt(2).*fitobject.slope)+fitobject.thresh); %for positive slope
end

% calculate confidence intervals
if length(level)>nAFC
    ci = confint(fitobject);
else
    ci=NaN;
end


if graphData
    if length(level)>nAFC
        ci95 = predint(fitobject,tLevel); % pull 95% confdence intervals from fitParams at each test level
    else
        ci95 = nan(2,length(tLevel));
    end
    errorbar(tLevel, pCorrect, dataErrEst, dataErrEst, 'bo');
    ylim([0,1]);
    hold on
    plot(fitobject, 'r-');
    if ~any(isnan(ci95(:))); % no NaNs in confidence intervals
        plot(tLevel,ci95,'g--'); % plot 95% confidence interval on fit line
    end
    %         hold off
    legend HIDE
    box off
    %         figure()
    %           plot(1./tLevel, pCorrect)
end

end

