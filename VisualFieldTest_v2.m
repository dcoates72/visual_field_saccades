%Visual field testing with LOG (laplacian-of-gaussian) stimuli
% saccade response: response provided with eye movements
% manual response: numeric keypad for response; if eye tracking flag is set to 1 (yes), then fixation monitoring is turned on and
% auditory feedback (buzzer noise) is provided for deviations from fixation (including eye blinks); those
% trials are replayed at a later time point

%contrast sensitivity testing (CSF) at each location: script is run twice; first to measure acuity, then contrast threshold at a lower spatial frequency
%Part1: Initial letter acuity is scaled for eccentricity, acuity is staircased
%Part2: Contrast threshold is staircased at a spatial frequency that is 1.16 log10 units below the acuity threshold

%escape to exit out (any time for manual responses, hold down during saccade task)

% 2018 CA + AK
% 2019 AK changes: Removed unused commented code, general tidying, changes to file
% naming system; eye tracking during fixation bloc ks

clear all;close all;clc
rng('Shuffle');
KbName('UnifyKeyNames');
%% start by setting testing parameters %%%%%%%

% user-defined parameters from dialog box
prompt = {'Subject initials:', 'nAFC:' 'Offset (0 primary, 1 diagonal)' 'Separate staircases for each angular location (y/n)?' 'Threshold Type (1 acuity, 2 contrast):', 'Manual or Saccade Response (m, s):', ...
    'Binocular, Left, or Right Eye Targets, for use with stereo mode (b, l, r):','gamma (R G B):','# Trials per staircase (Must be a multiple of nAFC if "separate staircases" set to "n"):', ...
    'Screen Width (cm)', 'Screen Height (cm)','Viewing Distance (cm)','Eye Tracking (0 No, 1 Yes)' 'Stereo mode:'};
dlg_title = 'Modern Visual Field';
num_lines = 1;
def = {'XX','8','0','y','2','s','b', '1.93 1.93 2.15', '27', '54', '30', '47', '1' '0'};
answer = inputdlg(prompt,dlg_title,num_lines,def); % read in parameters from GUI
sName=char(answer(1,1)); % subject ID for filename
nAFC = str2num(char(answer(2,1)));% number of angles to test (theta); for manual responses, select 2,4,or 8
offset = str2num(char(answer(3,1)));% changes the initial starting angle
separateStaircases = find(strcmpi(char(answer(4,1)),{'n' 'y'}))-1;%set to "y" estimate a threshold for each angular position (theta), or set to "n" to estimate just one threshold per eccentricity
acuityOrContrast=str2num(char(answer(5,1))); % set to 1 for acuity threshold, 2 for contrast
ResponseType=find(strcmpi(char(answer(6,1)),{'m' 's'})); % manual response or saccade response
EyeTested=find(strcmpi(char(answer(7,1)),{'b' 'l' 'r'})); % binocular (b), left(l) or right(r) eye
gammaVals=str2num(char(answer(8,1))); %gamma value for monitor linearization
nTrialsPerCond=str2num(char(answer(9,1))); % we used 27 trials, more is better
scrnWidthCm=str2num(char(answer(10,1))); %screen width (cm)
scrnHeightCm=str2num(char(answer(11,1))); % screen height (cm)
viewDistCm=str2num(char(answer(12,1))); % viewing distance (cm)
eyeTracking=str2num(char(answer(13,1))); % record eye movements or not
stereoMode=str2num(char(answer(14,1))); % for use with shutter glases; set to 0 to not do stereo presentation

% calculate pixels per degree
whichScreen=max(Screen('Screens')); % use highest display # for stimuli
resOutput = Screen('Resolution',whichScreen);%get screen resolution
scrnWidthDeg=2*atand((0.5*scrnWidthCm)/viewDistCm); % convert screen width to degrees visual angle
scrnHeightDeg=2*atand((0.5*scrnHeightCm)/viewDistCm);

% calculate pixels per degree
pixPerDegWidth=resOutput.width/scrnWidthDeg; % # pixels per degree
pixPerDegHeight=resOutput.height/scrnHeightDeg; % # pixels per degree
pixPerDegMean = mean([pixPerDegWidth pixPerDegHeight]);%take the average of horizontal & vertical measurements

% additional parameters:
% target parameters
% target locations
%%eccentricityDeg = [4.4 8.7 17.4];%[4 9 17];%degrees of visual angle; values actually used in the first experiment were 4.4�, 8.7�, 17.4�
eccentricityDeg = [5 10 15];
eccentricityPx= eccentricityDeg.*pixPerDegMean; % eccentricity converted to pixels

% figure out angles (and corresponding keys for manual responses)
thetaOffset = offset;% angles start at x o'clock (from dialogue box)
thetaInc = (2*pi)/nAFC;
theta = thetaOffset:thetaInc:((nAFC-1)*thetaInc+thetaOffset);
keysList = [6 3 2 1 4 7 8 9]; %mapping of theta angles to numeric keypad; ordered from theta (clockwise)
thetaKeys = 0:pi/4:(7*(pi/4));
myKeys = keysList(ismember(thetaKeys,theta)); %only really works for 2, 4, or 8 AFC; othwise need to configure keys manually

spatialConstant = 10;% for drawing LoG stimulus; size of image (in pixels) is spatialConstant*sigmaPx

% optional threshold adjustments for acuity block:
% in case the acuity estimate produces a very large or very small value
% (threshold stimulus larger than wedge, or psychometric function can't be
% fit, try averaging the reversals of the staircase)
clampAcuityEstimate = 1; % set to 0 to 1 to not clamp or to clamp

%%% optional  adjustments for stimulus size in contrast block:
%%% optional constraints so that at low SFs, stimulus doesn't go outside
%%% the size of the wedge that it covers
clampSFsInContrastBlock = 0; %0 or 1;constrain size of stimulus in contrast blocks
nSigmaClampMax = 4;% trough-to-trough width of LoG is ~4 sigma
% nSigmaClampMax = spatialConstant;% or clamp so that image (~10 sigma) doesn't go outside wedge

% these are the smallest and largest sigma value for the LoG; these
% constraints are used for both (1) constraining stimulus size in contrast block *BUT ONLY IF* clampSFsInContrastBlock is set to 1
% and (2) size of stimulus presented in acuity block; assume that acuity threshold is not larger than size of wedge (in ALL cases)
maxSigma = (2.*eccentricityPx.*tand((360/nAFC)/2))./nSigmaClampMax;  %Eccentricity and maxsigma in PIXELS
maxSigma = min([maxSigma; repmat(resOutput.height./nSigmaClampMax,1,length(maxSigma))]);

minSFDeg = (1./(maxSigma.*sqrt(2)*pi)).*pixPerDegMean; %in CPD; transform again from pixels to SFcpd
maxSFDeg = repmat(1./(sqrt(2).*pi.*(((1/4)./spatialConstant)./pixPerDegMean)),1,length(eccentricityPx));% set highest spatial frequency so that sigma is 1/4 of a pixel, or about 395 cpd (arbitrary & unlikely to encounter this limit; trough-to-trough would be ~1 pixel)

% %For troubleshooting; test only a subset of locations
% eccentricityDeg=(eccentricityDeg(3));
% theta= pi/2

% duration parameters
StimulusDurationNoSaccade=0.250; %average latency of a saccade in a detection task 250 ms
fixDuration = .5;% fixation duration
secondStimDur = .25;%duration of patch after saccade lands

%%% staircase parameters
% figure out staircase start point for acuity based on letter acuity from Anstis (1974)
ThresholdSizedegree=-0.031;%Foveal Theshold size(degree)
MagnificationFactorDegree=0.046;%'Magnification factor (degree)
SizeDegree= ThresholdSizedegree+(eccentricityDeg.*MagnificationFactorDegree); % y = mx + b
%the threshold is multiplied by 5 to start the staircase from higher value than threshold
staircaseStartSF= 2.5./(SizeDegree*5); %convert the threshold value to cycles per degree, to convert to cpd we multiply by 2.5 (number of cycles in Snellen letters)
staircaseStartContrast = .2; % start point for contrast staircases
initialStepSizes = [0.5 2]; %starting step sizes for acuity & contrast, respectively
stepSizesAfterReversal = [1./(10^0.1) (10^0.1)]; % step sizes for acuity & contrast after the third reversal, respectively

% fixation dot & background parameters
fixationSizes=[6 10]; %sizes of fixation point (pixels); before & during target
fixCol = [0 1]; % color of fixation dot for incorrect responses (0) and correct responses (1)
speedDegreesPerSecond = 20;
blue = 1; % don't have color on bitstealing mode
white = 1;

meanLum=0.50; % background screen level % TODO: MEasure Luminance to see if linear

% eye tracking parameters
calibAreaDeg = [38 32];%calibrated & tracked area, deg
calibProp = [.65 .85]*.75 %calibAreaDeg./[scrnWidthDeg scrnHeightDeg];%[.65 .85]*.9;  % custom calibration locations; proportion of screen area covered by calibration targets
calibAreaPx = calibAreaDeg.*pixPerDegMean;
calibTargSizePerc = 1; % increase calibration target size to 5% of screen size (default is 2.5% of screen size)
% to avoid having corrective saccades be counted as responses, add some
% constraints to what can be counted as a response:
minSaccLatency = 50;%min physical latency is 100-150, so this should include all possible saccades that are at correct timing
minSaccadeProportion = .25;% saccade amplitude must be some fraction of the distance to the nearest edge of the stimulus
absoluteSaccMin = 1;% make sure minimum saccade size isn't less than N deg
maxFixDist = 1.5*pixPerDegMean; % for manual responses & eye tracking, subjects need to maintain fixation within 1.5 deg of fixation dot

spot_radius = 10; % pixels. TODO: specify in degrees of visual angle

breakFrequency = 162; %break every N trials

% labels for file name
blockLabels = {'Acuity' 'Contrast'};
eyeLabels = {'Binocular' 'Left' 'Right'};
eyeTestedBuffer = {[0 1], 0, 1};%for use with Screen('SelectStereoDrawBuffer'); binocular, left, and right eye buffers, respsectively
respType = {'ManualResp' 'SaccadeResp'};
% set filename; adding timestamp prevents old files from being overwritten
dataFile = [sName '_' num2str(nAFC) 'AFC_' blockLabels{acuityOrContrast} '_' eyeLabels{EyeTested} '_' respType{ResponseType} '_' mfilename '_' datestr(now,'mmddyyyy_HHMMSS') '.mat']; %filename

%set up the counterbalance
EccentricityConds=length(eccentricityPx);
if separateStaircases
    ThetaConds=length(theta);
else
    ThetaConds = 1;
end

if acuityOrContrast == 1;%acuity
    sfTestLevels = 0;
else
    answerSF = -1.16;
    %       inputdlg('Spatial frequencies to test (log10 units relative to acuity threshold, separate using spacebar):','SFs for contrast threshold',1,{'-1.16'});
    sfTestLevels = [-1.16]; 
    %       str2num(char(answerSF{1}));
    %     sfTestLevels = [ -2 -1.7 -1.43 -1.16 -.58];%[-1.16];%values are log10 units relative to acuity
end

%set up the counterbalance
if separateStaircases
    [randomCondEccentricity, randomCondTheta, randomCondSF] = BalanceFactors(nTrialsPerCond, 1, 1:EccentricityConds, 1:ThetaConds,1:length(sfTestLevels));
else
    [randomCondEccentricity, randomCondTheta, randomCondSF,randomTargLocation] = BalanceFactors(nTrialsPerCond./nAFC, 1, 1:EccentricityConds, 1:ThetaConds , 1:length(sfTestLevels),1:nAFC);
end

nTrialsTotal = size(randomCondEccentricity,1);

%% for the contrast sensitivity blocks, fit the acuity data to figure out which spatial frequency to test

pFuncOld=[];
if acuityOrContrast==2
    %[myFile,myPath]= uigetfile({[sName '_*' blockLabels{1}, '*' mfilename '*.mat']});
    
    myFile='test_8AFC_Acuity_Binocular_SaccadeResp_VisualFieldTest_v2_07102019_151309.mat';
    myPath='.';
    load([myPath '/' myFile], 'pFunc');
    pFuncOld=pFunc;
    AcuityThresSFStored= nan(EccentricityConds,ThetaConds);
    originalSF=nan(EccentricityConds,ThetaConds,length(sfTestLevels));
    adjustedSF=nan(EccentricityConds,ThetaConds,length(sfTestLevels));
    subjSigmaEstimateDeg=nan(EccentricityConds,ThetaConds,length(sfTestLevels));
    subjSigmaEstimatePx=nan(EccentricityConds,ThetaConds,length(sfTestLevels));
    for ECC_Index=1:EccentricityConds
        for THETA_Index=1:ThetaConds
            %             nAFC= ThetaConds; %detection task on radial locations
            graphData=0;
            
            threshLevel = .8;
            [fitobject,gof,~,AcuityThresLogSF] = fitPFuncnAFC_v2(log10(pFuncOld{ECC_Index, THETA_Index}.level(1:(pFuncOld{ECC_Index, THETA_Index}.nPresented-1))), pFuncOld{ECC_Index, THETA_Index}.response, nAFC, graphData,1,threshLevel);%fit the previously collected acuity data; x-axis is 1/SF (slope of function is positive)
            AcuityThresSF = 10^AcuityThresLogSF;
            
            % if the function can't be fit (gives an extreme value), try averaging reversals (or trials)
            if clampAcuityEstimate==1 % constrain the values...
                if (AcuityThresSF<minSFDeg(ECC_Index)  || AcuityThresSF >maxSFDeg(ECC_Index)) && sum(pFuncOld{ECC_Index, THETA_Index}.revLevel)>0
                    AcuityThresSF = 10.^mean(log10(pFuncOld{ECC_Index, THETA_Index}.revLevel(pFuncOld{ECC_Index, THETA_Index}.revLevel~=0)));
                    %if there is no reversal (unlikely), try the average of the last 10 values, just so we have something to use....
                elseif (AcuityThresSF<minSFDeg(ECC_Index)  || AcuityThresSF >maxSFDeg(ECC_Index)) && sum(pFuncOld{ECC_Index, THETA_Index}.revLevel)==0
                    AcuityThresSF = 10.^mean(log10(pFuncOld{ECC_Index, THETA_Index}.level(end-10:end)));
                end
                % if it's still outside these bounds, constrain the values
                AcuityThresSF = min([AcuityThresSF maxSFDeg(ECC_Index)]);%make sure spatial frequency doesn't exceed maxSFDeg
                AcuityThresSF = max([AcuityThresSF minSFDeg(ECC_Index)]);%make sure spatial frequency doesn't fall below minSFDeg
            end
            
            AcuityThresSFStored(ECC_Index,THETA_Index) = AcuityThresSF; 
            
            %  set the spatial frequency levels
            for sfIndex = 1:length(sfTestLevels)
                TestSF = 10.^(log10(AcuityThresSF)+sfTestLevels(sfIndex));
                TestSF = 0.2;% Did this to make equal-sized target (DC)
                
                originalSF(ECC_Index, THETA_Index,sfIndex) = TestSF;
                
                if clampSFsInContrastBlock==1 % constrain the values...
                    TestSF = min([TestSF maxSFDeg(ECC_Index)]);%make sure spatial frequency doesn't exceed maxSFDeg
                    TestSF = max([TestSF minSFDeg(ECC_Index)]);%make sure spatial frequency doesn't fall below minSFDeg
                end
                
                %TestSF is in cycles per degree, convert to sigma in pixels
                %to transform cycle per degree to pixels
                sigma=1/(TestSF*sqrt(2)*pi);%sigma in degrees;
                %                 sigmaPx=min([resOutput.height/spatialConstant sigma*pixPerDegMean]);%sigma in pixels
                sigmaPx=sigma*pixPerDegMean;%sigma in pixels
                
                adjustedSF(ECC_Index, THETA_Index,sfIndex) = TestSF;
                subjSigmaEstimateDeg(ECC_Index, THETA_Index,sfIndex) = sigma;
                subjSigmaEstimatePx(ECC_Index, THETA_Index,sfIndex) = sigmaPx;
            end
        end
    end
    
end

% TODO: See what value is coming from the existing acuity file. For now, we'll just
% hard-code for the size we want.
%
% Best to just use a circle of a given radius in visual angle
%

%%
% set up the staircases using pFunc
pFunc=repmat({struct()},EccentricityConds,ThetaConds,length(sfTestLevels));
for EccentricitycondNo=1:EccentricityConds
    for ThetacondNo=1:ThetaConds
        for sfIndex = 1:length(sfTestLevels)
            if  acuityOrContrast==1 %acuity, 1-down, 3-up
                pFunc{EccentricitycondNo, ThetacondNo,sfIndex} = setUpPFunc_v2(nTrialsPerCond, nTrialsPerCond, staircaseStartSF(EccentricitycondNo), 1, 1, 3, initialStepSizes(acuityOrContrast)); % 1 wrong then go up, 3 right then go down,  staircase
            else %contrast;  1-down, 3-upp
                pFunc{EccentricitycondNo, ThetacondNo,sfIndex} = setUpPFunc_v2(nTrialsPerCond, nTrialsPerCond, staircaseStartContrast, 1, 1, 3, initialStepSizes(acuityOrContrast)); % 1 wrong then go up, 3 right then go down,  staircase
            end
        end
    end
end

%% run the experiment
commandwindow;

try 
% try is hiding errors (DC)
    if eyeTracking; % set up and calibrate eye tracker
        
        if (Eyelink('Initialize') ~= 0), return; % check eye tracker is live
        end;
        
        [windowPtr, winRect] = Screen('OpenWindow', whichScreen, 127, []); % open simple window for eye track calibration (allows presentation of binocular targets when using shutter glasses)
        
        el=EyelinkInitDefaults(windowPtr); % initialize eye tracker default settings
        if ~EyelinkInit(0)
            fprintf('Eyelink Init aborted.\n');
            cleanup;  % cleanup function
            return;
        end
        el.calibrationtargetsize=calibTargSizePerc; % increase calibration target size (default is 2.5% of screen size)
        
        Eyelink('command', 'link_sample_data = LEFT,RIGHT,GAZE,AREA');
        Eyelink('command',['calibration_area_proportion = ' num2str(calibProp)]);
        Eyelink('command',['validation_area_proportion = ' num2str(calibProp)]);
        Eyelink('Command', 'file_event_data = GAZE,GAZEREZ,AREA,HREF,VELOCITY');
        Eyelink('Command', 'link_sample_data = GAZE,GAZEREZ,AREA,HREF,VELOCITY');
        Eyelink('Command', 'link_event_data = GAZE,GAZEREZ,AREA,HREF,VELOCITY');
        
        edfFile='demo.edf'; % file gets renamed later
        Eyelink('Openfile', edfFile)
        
        EyelinkDoTrackerSetup(el);
        
        eyeUsed = Eyelink('EyeAvailable'); % get eye that's tracked
        Screen('Close', windowPtr);
    else
        EyelinkInit(1);
    end
    
    %  initial setup functions, open a window
    Screen('Preference', 'SkipSyncTests', 1);
    KbName('UnifyKeyNames');
    PsychImaging('PrepareConfiguration');   % set up imaging pipeline
    PsychImaging('AddTask', 'General', 'FloatingPoint32BitIfPossible'); % request 32 bit per pixel for high res contrast
    PsychImaging('AddTask', 'General', 'NormalizedHighresColorRange', 1);
    PsychImaging('AddTask', 'FinalFormatting', 'DisplayColorCorrection', 'SimpleGamma'); % gamma correction
    PsychImaging('AddTask', 'General', 'EnablePseudoGrayOutput'); % bit stealing
    
    [windowPtr, winRect] = PsychImaging('OpenWindow', whichScreen, meanLum, [],[],[],stereoMode);    % open experimental window with required parameters
    HideCursor;
    
    Screen('BlendFunction', windowPtr, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);  % enable alpha blending
    
    PsychColorCorrection('SetEncodingGamma', windowPtr, 1./gammaVals); % fix gamma
    
    if (max(eccentricityPx))>(RectHeight(winRect)/2)%
        sca;error('Eccentricity out of bounds');
    end
    
    [centerX,centerY] = RectCenter(winRect);% center of display
    
    frameRate=Screen('FrameRate', windowPtr); % screen timing parameters
    if frameRate==0; frameRate=60; end % catch bug in some systems
    
    Screen('TextSize',windowPtr,16);                               % put some instructions on Screen
    Screen('TextFont',windowPtr,'Arial');
    textToObserver=sprintf('Screen Parameters %d by %d pixels at %3.2f Hz. Hit mouse button to start', winRect(3)-winRect(1),winRect(4)-winRect(2), frameRate);
    for eye = 0:1;
        Screen('SelectStereoDrawBuffer', windowPtr, eye); % select left eye screen
        Screen('DrawText', windowPtr, textToObserver, 100, 100, 0, meanLum);
        Screen('FillOval',windowPtr,fixCol(1),CenterRect(SetRect(0,0,fixationSizes(1),fixationSizes(1)),winRect));
    end
    lastflip=Screen('Flip', windowPtr);% switch to start Screen with instructions
    
    
    [mx,my,buttons] = GetMouse; % wait for mouse button release before starting experiment
    while any(buttons) % if already down, wait for release
        [mx,my,buttons] = GetMouse(windowPtr);
    end
    while ~any(buttons) % wait for new press
        [mx,my,buttons] = GetMouse(windowPtr);
    end
    
    tstart = tic;
    if eyeTracking; % start recording
        Eyelink('StartRecording');
        Eyelink('Message', 'SYNCTIME'); % mark zero-plot time in data file
        eyeUsed = Eyelink('EyeAvailable'); % get eye that's tracked
        recalibrateSubj = 0;
    end
    
    % initialize some variables
    respCorrect = 0;% just sets the fixation color to black for the first strial;
    fixCheck = 1;
    nValidTrials = 0;
    trialCount = 0;
    exitExp = 0;
    while nValidTrials < nTrialsTotal% work through all trials
        
        trialCount = trialCount+1;
        HideCursor; %hide cursor in case it re-appears
        
        shuffleTries=50;
        
        while shuffleTries>0;
            % Until we find a nice on-screen target, keep shuffling the
            % list of trial conditions..
            
            % pull out indices for each stimulus condition from counterbalanced list of conditions
            ECC_Index=randomCondEccentricity(trialCount);
            THETA_Index=randomCondTheta(trialCount);
            sfIndex = randomCondSF(trialCount);
            nPresented = pFunc{ECC_Index, THETA_Index, sfIndex}.nPresented;
            level= pFunc{ECC_Index, THETA_Index, sfIndex}.level;

            % figure out size and contrast of LoG for this trial
            if acuityOrContrast==1 %acuity
                % pFunc will use spatial frequency (in cycles per degree), we
                % need to figure out what SigmaPixel size that corresponds to
                % staircaseStartSF used in the staircase
                level(nPresented) = min([level(nPresented) maxSFDeg(ECC_Index)]);
                level(nPresented) = max([level(nPresented) minSFDeg(ECC_Index)]);

                TestSF = level(nPresented);
                SFcpp = (TestSF./pixPerDegMean); %cycles per pixel; should be ((1/levelSFcpd)*pixPerDeg)
                sigmaTrial=1/(SFcpp*sqrt(2)*pi); %Sigma Pixel size from SFcpp; to find the sigma of the LoG corresponding to SF we need to multiply SF for a contant (sqrt(2)*pi)

                trialContrast = 1; % stimuli drawn at 100% contrast in the acuity block

            elseif acuityOrContrast==2 % contrast

                sigmaTrial = subjSigmaEstimatePx(ECC_Index, THETA_Index, sfIndex); % set sigma based on selected spatial frequency

                % set contrast; can't go outside 0 - 100%
                level(nPresented) = min([level(nPresented) 1]);
                level(nPresented) = max([level(nPresented) 0]);
                trialContrast = level(nPresented);
            end

            pFunc{ECC_Index, THETA_Index, sfIndex}.level(nPresented) = level(nPresented); %in case the values have been clamped, put the values back into pFunc

            EffectiveStimulusSizePix=(sigmaTrial*spatialConstant); % height of rect for drawing LoG

            % set stimulus center in pixels
            if separateStaircases
                [xpix(trialCount),ypix(trialCount)] = pol2cart(theta(THETA_Index),eccentricityPx(ECC_Index));
            else
                [xpix(trialCount),ypix(trialCount)] = pol2cart(theta(randomTargLocation(trialCount)),eccentricityPx(ECC_Index));
            end

            minSaccadeSize = (eccentricityPx(ECC_Index)-(EffectiveStimulusSizePix/2))*minSaccadeProportion; %saccade size is proportional to SFSize (SF of the stimulus) which changes with eccentricity
            minSaccadeSize = max([minSaccadeSize pixPerDegMean*absoluteSaccMin]);

            %%%%%%% what happens if next trial stimulus is outside the screen border? Move the fixation to the center.
            stimulusBounds = CenterRect(GrowRect([0 0 calibAreaPx],-(fixationSizes(2)),-(fixationSizes(2))),winRect); %bounds of the tracked area
            centerX = min([centerX stimulusBounds(3)]);centerX = max([centerX stimulusBounds(1)]);
            centerY = min([centerY stimulusBounds(4)]);centerY = max([centerY stimulusBounds(2)]);
            oldCoordinates = [centerX+xpix(trialCount), centerY+ypix(trialCount)]; %coordinates of the following trial

            % set a path for the dot to move back to the screen
            trajectory=[];
            theta22=[];

            if ~IsInRect(oldCoordinates(1),oldCoordinates(2),stimulusBounds);
                [theta22,rho22 ]= cart2pol(centerX-mean([winRect(1) winRect(3)]), centerY-mean([winRect(2) winRect(4)])); %
                nFramesMovingDot = round(((rho22/pixPerDegMean)/speedDegreesPerSecond)*frameRate);
                rhoList = linspace(rho22,0,nFramesMovingDot);
                [trajectoryX,trajectoryY] = pol2cart(theta22,rhoList);
                trajectory = [trajectoryX;trajectoryY];
            end
            
            % TODO: simplify by combining above and below: is target in
            % rect? if not, do the shuffle
            
             if ~isempty(trajectory)
                 % Means that the currently selected target is off screen and needs to re-center
                 % Instead, we will shuffle the remaining trials to find
                 % one that will work (be on screen).
                indexes=nTrialsTotal:trialCount;
                shufd=Shuffle(indexes);
                randomCondTheta(indexes)=randomCondTheta(shufd);
                randomCondEccentricity(indexes)=randomCondEccentricity(shufd);
                randomCondSF(indexes)=randomCondSF(shufd);
                
                shuffleTries = shuffleTries-1;
             else
                 % If no trajectory, we can continue!
                 break;
             end % if ~isempty(trajectory)
        end % while
             
        % Stay in the while loop since we are not done

        %%%%%%%%%%%%
        
        %shows the fixation dot
        if ~isempty(trajectory) % move it back to the center if necessar
                      
            %TO SHOW THE DIRECTION WHERE THE STIMULUS WOULD BE OUTSIDE THE SCREEN
            %                 Screen('DrawLine', windowPtr ,0, centerX, centerY, oldCoordinates(1), oldCoordinates(2),5);
            %                 Screen('Flip',windowPtr);
            %                 WaitSecs(2);
            
            for frame = 1:nFramesMovingDot
                [centerX,centerY] = RectCenter(winRect);% center of display
                Screen('SelectStereoDrawBuffer', windowPtr, 0); % select left eye screen
                Screen('FillOval',windowPtr,blue,(CenterRectOnPointd(SetRect(0,0,fixationSizes(1),fixationSizes(1)),centerX+trajectory(1,frame),centerY+trajectory(2,frame))));
                Screen('SelectStereoDrawBuffer', windowPtr, 1); % select left eye screen
                Screen('FillOval',windowPtr,blue,(CenterRectOnPointd(SetRect(0,0,fixationSizes(1),fixationSizes(1)),centerX+trajectory(1,frame),centerY+trajectory(2,frame))));
                lastflip =Screen('Flip',  windowPtr);
            end
            
        else
            
            Screen('SelectStereoDrawBuffer', windowPtr, 0); % select left eye screen
            Screen('FillOval',windowPtr,blue,(CenterRectOnPointd(SetRect(0,0,fixationSizes(1),fixationSizes(1)),centerX,centerY))); %offset center to left up corner then add raw EyePosition
            Screen('SelectStereoDrawBuffer', windowPtr, 1); % select right eye screen
            Screen('FillOval',windowPtr,blue,(CenterRectOnPointd(SetRect(0,0,fixationSizes(1),fixationSizes(1)),centerX,centerY))); %offset center to left up corner then add raw EyePosition
            lastflip = Screen('Flip',windowPtr,lastflip+secondStimDur); % flip to fixation dot
            if eyeTracking
                Eyelink('Message','FIXONSET');
            end
        end
        

        % if monitoring fixation during MANUAL response block
        if ResponseType==1 && eyeTracking %wait for subject to look at the fixation dot (1 sample) before showing the target
            distFromFix = Inf;  xLoc=0; yLoc = 0;
            while distFromFix>maxFixDist
                if Eyelink( 'NewFloatSampleAvailable') > 0
                    currData=Eyelink('NewestFloatSample');
                    if eyeUsed == el.BINOCULAR; % if both eyes are tracked
                        xLoc=mean(currData.gx); % drop the current eye location to the variables x_loc, y_loc
                        yLoc=mean(currData.gy);
                    else
                        xLoc=currData.gx(eyeUsed+1); % drop the current eye location to the variables x_loc, y_loc
                        yLoc=currData.gy(eyeUsed+1);
                    end
                end
                
                distFromFix = sqrt((xLoc-(centerX)).^2+(yLoc-(centerY)).^2);
                
                % a couple options if stuck
                [keyIsDown,seconds,keyCode] = KbCheck;
                if strcmpi(KbName(keyCode),'escape'); % exit the experiment
                    exitExp =1;
                    break;
                end
                if strcmpi(KbName(keyCode),'c'); % recalibrate
                    Eyelink('StopRecording');
                    
                    [windowPtr1, winRect] = Screen('OpenWindow', whichScreen, 127, []); % open simple window for eye track calibration
                    el=EyelinkInitDefaults(windowPtr1); % initialize eye tracker default settings
                    
                    EyelinkDoTrackerSetup(el); %hands control off to the eyetracker for calibration
                    while keyIsDown % wait for key to be released (otherwise holding down 'esc' may exit out of experiment)
                        [keyIsDown,seconds,keyCode] = KbCheck;
                    end
                    Screen('Close', windowPtr1);
                    Eyelink('StartRecording');
                end
                %                 Screen('FillOval',windowPtr,fixCol(respCorrect+1),(CenterRectOnPointd(SetRect(0,0,fixationSizes(2),fixationSizes(2)),centerX,centerY))); %offset center to left up corner then add raw EyePosition
                %                 Screen('FillOval',windowPtr,255,(CenterRectOnPointd(SetRect(0,0,fixationSizes(2),fixationSizes(2)),xLoc,yLoc))); %offset center to left up corner then add raw EyePosition
                %                 Screen('DrawText',windowPtr, num2str(distFromFix),xLoc,yLoc,255); %offset center to left up corner then add raw EyePosition
                %
                %                 lastflip = Screen('Flip',windowPtr,lastflip); % flip to stimulus image
                %
            end
        end
        
        targetX=centerX+xpix(trialCount);
        targetY=centerY+ypix(trialCount);
        
        % Draw a simple circle. Who knew it was so hard?
        baseRect = [0 0 2*spot_radius 2*spot_radius];
        % For Ovals we set a miximum diameter up to which it is perfect for
        maxDiameter = max(baseRect) * 1.01;
        % Center the rectangle on the centre of the screen
        centeredRect = CenterRectOnPointd(baseRect, targetX, targetY);
        % Set the color of the rect to red
        targetColor = 0.5+0.5*trialContrast;
        % Draw the rect to the screen
        %Screen('FillOval', windowPtr, rectColor, centeredRect, maxDiameter);
        
        %%%%% ALL CONDITIONS: DRAW THE TARGET & FIXATION DOT
        destRect=CenterRectOnPointd(SetRect(0,0,EffectiveStimulusSizePix,EffectiveStimulusSizePix),centerX+xpix(trialCount),centerY+ypix(trialCount)); % work out on-screen co-ordinates for this stimulus
        
        for eyeShown = eyeTestedBuffer{EyeTested} % draw target to left, right, or both eyes
            Screen('SelectStereoDrawBuffer', windowPtr, eyeShown)
            

            % Draw the rect to the screen
            Screen('FillOval', windowPtr, targetColor, centeredRect, maxDiameter);
        
            % Screen('DrawTexture', windowPtr, targetTex, [], destRect); % draw texture to on screen location
            %                 Screen('FrameRect', windowPtr, 255, destRect); % draw texture to on screen location
        end
        
        for eyeShown = 0:1; %draw fixation dot binocularly
            Screen('SelectStereoDrawBuffer', windowPtr, eyeShown);
            Screen('FillOval',windowPtr,fixCol(respCorrect+1),(CenterRectOnPointd(SetRect(0,0,fixationSizes(2),fixationSizes(2)),centerX,centerY)));  %draw fixation dot in the center
        end
        
        
        lastflip=Screen('Flip',windowPtr,lastflip+fixDuration); % flip to stimulus image
        
        
        stimTimer  = tic;
        if eyeTracking % if eyetracking, send message to indicate stimulus onset & get current time
            Eyelink('Message','STIMONSET');
            %         stimOnsetTime = Eyelink('TrackerTime');
            %         stimOnsetTime = (Eyelink('TimeOffset')+Eyelink('TrackerTime'));
            sample = Eyelink('NewestFloatSample');
            stimOnsetTime = sample.time;
        end
        
        onTime = GetSecs; % Record the time that stimulus appeared
        
        %%%%% EVENTS AFTER STIULUS APPEARANCE, DIFFERENT FOR EACH TYPE OF RESPONSE %%%%%%%%%%%%
        
        % initialize variables (used for saccade responses; empty otherwise)
        evt.time = nan;
        evtSacc = [];
        evtype = 0;
        allPositions = [nan nan;nan nan];
        eyePos = struct('OSX',[],'ODX',[],'OSY',[],'ODY',[],'time',[]);
        allSaccStore = struct('evtSacc',[],'saccLatency',[],'saccSize',[],'minSaccSize',[],'minSaccLatency',[]);
        
        
        % assumes [respAngleRadians,saccSize] gotten from saccade
        if ResponseType==2; % saccade responses
            
            while (GetSecs-onTime) < 1.5 % NEW: timeout after 1.5 secs of not moving
                
                %check for a saccade above a certain size
                
                error=Eyelink('CheckRecording'); % check that Eyelink is still recording
                if(error~=0); break; end
                
                if evtype ~= 0 && evtype~= el.SAMPLE_TYPE
                    prevEvent = evtype;
                end
                evtype = Eyelink('GetNextDataType');
                if evtype == el.ENDSACC && prevEvent ~= el.ENDBLINK && prevEvent ~= el.LOSTDATAEVENT % if it's the end of a saccade (& not the end of an eyeblink)
                    evtSacc = Eyelink('getfloatdata',evtype);
                    [respAngleRadians,saccSize] = cart2pol((evtSacc.genx-evtSacc.gstx),(evtSacc.geny-evtSacc.gsty));
                    saccLatency = evtSacc.sttime-stimOnsetTime;
                    
                    % store information about all recorded saccades
                    allSaccStore.evtSacc = [allSaccStore.evtSacc evtSacc];
                    allSaccStore.saccLatency = [allSaccStore.saccLatency saccLatency];
                    allSaccStore.saccSize = [allSaccStore.saccSize saccSize];
                    allSaccStore.minSaccSize = [allSaccStore.minSaccSize minSaccadeSize];
                    allSaccStore.minSaccLatency = [allSaccStore.minSaccLatency minSaccLatency];
                    
                    if saccSize>minSaccadeSize && saccLatency>minSaccLatency
                        break;
                    end
                end
                
                if evtype== el.SAMPLE_TYPE; % get the sample in the form of an event structure
                    evt =  Eyelink('getfloatdata',evtype);%Eyelink('NewestFloatSample');
                    allPositions = [evt.gx;evt.gy];
                    allPositions(allPositions==el.MISSING_DATA) = nan;
                    
                    % grow eye position data record
                    eyePos.OSX=[eyePos.OSX allPositions(1,1)];
                    eyePos.ODX=[eyePos.ODX allPositions(1,2)];
                    eyePos.OSY=[eyePos.OSY allPositions(2,1)];
                    eyePos.ODY=[eyePos.ODY allPositions(2,2)];
                    eyePos.time=[eyePos.time evt.time];
                end
                
                % Could fall into out here if no movement for 1.5s
            end
            
            if eyeTracking
                Eyelink('Message','SACCEND');
            end
            
            % once saccade lands; show patch wherever they landed their eyes
            %                     and add the next fixation dot
            % show stimulus at the saccade landing location
            
            if ~isempty(evtSacc)
                centerX = evtSacc.genx;%centerX+xpix(trialCount);%
                centerY = evtSacc.geny;%centerY+ypix(trialCount);%
            else 
                disp('No Saccade, keeping previous center');
                centerX = centerX;%previous value
                centerY = centerY;%previous value 
                % TODO? respAngleRadians,saccSize
            end
            
            for eyeShown = eyeTestedBuffer{EyeTested}
                Screen('SelectStereoDrawBuffer', windowPtr, eyeShown); % select left eye screen
                
              % Draw a simple circle. Who knew it was so hard?
                baseRect = [0 0 2*spot_radius 2*spot_radius];
                % For Ovals we set a miximum diameter up to which it is perfect for
                maxDiameter = max(baseRect) * 1.01;
                % Center the rectangle on the centre of the screen
                centeredRect = CenterRectOnPointd(baseRect, centerX, centerY);
                % Draw the circle to the screen
                Screen('FillOval', windowPtr, white, centeredRect, maxDiameter);
        
                %Screen('DrawTexture', windowPtr, targetTex, [],CenterRectOnPointd(SetRect(0,0,EffectiveStimulusSizePix,EffectiveStimulusSizePix),centerX,centerY)); % draw texture to on screen location
            end
            
            % optional: SHOWS TEXT, LINE and DOTS for debugging purposes
            %             Screen('DrawText',windowPtr,num2str(mod(respAngleRadians,2*pi)),centerX,centerY);
            %             if ~isempty(eyePos.OSX)
            %                 Screen('DrawLine', windowPtr ,0, evtSacc.gstx, evtSacc.gsty, evtSacc.genx, evtSacc.geny);
            
            %                 saccadePts = eyePos.time>evtSacc.sttime & eyePos.time<evtSacc.entime;
            %                 Screen('DrawDots', windowPtr, [eyePos.OSX(saccadePts);eyePos.OSY(saccadePts)],5)
            %                 Screen('DrawDots', windowPtr, [eyePos.ODX(saccadePts);eyePos.ODY(saccadePts)],5)
            %             end
            lastflip = Screen('Flip',windowPtr); % flip to stimulus image in new location
            
        elseif ResponseType==1 && ~eyeTracking  %Response Type is 1, manual response & no eye tracking
            lastflip = Screen('Flip',windowPtr,lastflip+StimulusDurationNoSaccade); % stimulus goes away after StimulusDurationNoSaccade,nothing else happens
            
        elseif  ResponseType==1 && eyeTracking % Response Type is 1, manual response & eye tracking - monitoring fixation
            fixCheck = 1;
            while toc(stimTimer)<StimulusDurationNoSaccade %stimulus goes away after StimulusDurationNoSaccade, but fixation is monitored continuously until it does
                xLoc=nan; yLoc = nan;
                if Eyelink( 'NewFloatSampleAvailable') > 0
                    currData=Eyelink('NewestFloatSample');
                    if eyeUsed == el.BINOCULAR; % if both eyes are tracked
                        xLoc=mean(currData.gx); % drop the current eye location to the variables x_loc, y_loc
                        yLoc=mean(currData.gy);
                    else
                        xLoc=currData.gx(eyeUsed+1); % drop the current eye location to the variables x_loc, y_loc
                        yLoc=currData.gy(eyeUsed+1);
                    end
                end
                distFromFix = sqrt((xLoc-(centerX)).^2+(yLoc-(centerY)).^2);
                if distFromFix>maxFixDist %if eye position deviates from fixation, either saccade or an eye blink, fixCheck set to 0, trial is discarded & replayed later
                    fixCheck = 0;
                    if eyeTracking
                        Eyelink('Message','FIXCHECK0');
                    end
                    break;
                end
            end
            lastflip = Screen('Flip',windowPtr); % flip to blank screen
            if fixCheck==0; %buzzer noise if there's a deviation from fixation
                beepDur = .25;Fs = 48000;
                Snd('Play',abs(sum(sin(repmat([140 280 560]',1,length((0:beepDur*Fs))).*repmat(2*pi*(0:beepDur*Fs)/Fs,3,1)))),Fs)
            end
        end
        
        if eyeTracking
            Eyelink('Message','STIMOFFSET');
        end
        
        %%%% COLLECT RESPONSE, DETERMINE IF IT WAS CORRECT
        
        if separateStaircases
            correctAnswer = THETA_Index;
        else
            correctAnswer = randomTargLocation(trialCount);
        end
        
        if ResponseType==2 %classify response into nAFC response
            distancesFromWedgeCenter = angle(exp(1i*(respAngleRadians))./exp(1i*theta)); %circular distance between response & center of each wedge
            distanceFromCorrect = angle(exp(1i*(respAngleRadians))./exp(1i*theta(correctAnswer))); %circular distance between response & center of each wedge
            [~,saccResp] = min(abs(distancesFromWedgeCenter));% categorizes saccade response into nAFC response based on which wedge center the saccade is closest to
           %if saccResp == correctAnswer  % 1,2,3,4,5,6,7,8    for Theta= [0 pi/4 pi/2 3*pi/4 pi 5*pi/4 6*pi/4 7*pi/4];
           if abs(distanceFromCorrect) < pi/4
                respCorrect = 1;
            else
                respCorrect = 0;
            end
            choice = saccResp;
        else %%% manual response: record keypress using numeric keypad
            FlushEvents('keydown');
            choice = [];
            
            keyTime=GetSecs;
            while (GetSecs-keyTime) < 1.5 
                [keyIsDown, secs, keyCode] = KbCheck;
                if keyIsDown;
                    keyPressed = KbName(keyCode);
                    if ~iscell(keyPressed); keyPressed = {keyPressed};end
                    choice = str2num(keyPressed{1}(1));
                    if ismember(choice,myKeys) % A good key was pressed
                        break;
                    end
                    if strcmpi(keyPressed{1},'escape')
                        exitExp = 1;
                        break;
                    end
                end
            end
            
            if isempty(choice)
                choice=-1;
                keyPressed='0';
            end
            
            % determine if response is correct
            %using the numeric pad
            if choice == myKeys(correctAnswer);
                respCorrect = 1;
            else
                respCorrect = 0;
            end
            
            FlushEvents('keydown'); % DC/VP: fix 2-button problem?
            
            lastflip = Screen('Flip',windowPtr);
        end
        %7
        
        % update the staircase
        nValidTrials = nValidTrials + fixCheck;
        if fixCheck == 1;%only update staircase if fixCheck == 1
            pFunc{ECC_Index, THETA_Index,sfIndex} = updatePFunc_v2(pFunc{ECC_Index, THETA_Index, sfIndex}, respCorrect, stepSizesAfterReversal(acuityOrContrast), acuityOrContrast); % update staircase and save response time
        else   %%% if it's a bad trial, replay one from the same condition later on
            insertTrial = trialCount+randi(length(randomCondEccentricity)-trialCount+1)-1;
            
            randomCondEccentricity = [randomCondEccentricity(1:insertTrial);ECC_Index;randomCondEccentricity((insertTrial+1):end)];
            randomCondTheta = [randomCondTheta(1:insertTrial);THETA_Index;randomCondTheta((insertTrial+1):end)];
            randomCondSF = [randomCondSF(1:insertTrial);sfIndex;randomCondSF((insertTrial+1):end)];
            if ~separateStaircases
                randomTargLocation = [randomTargLocation(1:insertTrial);randomTargLocation(trialCount);randomTargLocation((insertTrial+1):end)];
            end
            
        end
        
        % pause screen every 100 trials (set by breakFrequency)
        if (rem(nValidTrials,breakFrequency)) == 0 && nValidTrials ~= (nTrialsTotal) && nValidTrials > 0
            WaitSecs(.5)
            for eye = 0:1
                Screen('SelectStereoDrawBuffer', windowPtr, eye)
                DrawFormattedText(windowPtr,[num2str(nValidTrials) ' out of ' num2str((nTrialsTotal)) ' trials completed. Press Space Bar to continue without re-calibrating. Press C to recalibrate and continue.'],'center','center',255);
            end
            Screen('Flip', windowPtr);
            
            [keyIsDown,seconds,keyCode] = KbCheck;
            while 1
                [keyIsDown,seconds,keyCode] = KbCheck;
                if ~iscell(KbName(keyCode))
                    keysPressed = {KbName(keyCode)};
                else
                    keysPressed = KbName(keyCode);
                end
                if strcmpi(keysPressed{1},'c');
                    recalibrateSubj = 1;
                    break;
                elseif strcmpi(keysPressed{1},'space');
                    recalibrateSubj = 0;
                    break;
                end
            end
            while keyIsDown [keyIsDown,seconds,keyCode] = KbCheck; end % wait for key release
            for eyeShown = 0:1
                Screen('SelectStereoDrawBuffer', windowPtr, eyeShown); % select left eye screen
                Screen('FillOval',windowPtr,fixCol(respCorrect+1),(CenterRectOnPointd(SetRect(0,0,fixationSizes(1),fixationSizes(1)),centerX,centerY))); %offset center to left up corner then add raw EyePosition
            end
            lastflip = Screen('Flip', windowPtr); %draw fixation dot for one additional second
            tic;while toc<.5; end;
        end
        
        if eyeTracking %%% there's a chance to re-calibrate at the end of each trial if desired, but key press is only checked once, so key has to be held down
            %9
            if separateStaircases
                Eyelink('Message', sprintf('Eccent%dTheta%d', ECC_Index, THETA_Index)); % record condition and trial in eye tracking data file
            else
                Eyelink('Message', sprintf('Eccent%dTheta%d', ECC_Index, randomTargLocation(trialCount))); % record condition and trial in eye tracking data file
            end
            
            [keyIsDown,seconds,keyCode] = KbCheck; % check for key press
            KbName(keyCode)
            if strcmpi(KbName(keyCode),'escape'); % exit out of the experiment entirely by pressing 'escape'
                stopExp =1;sca; %screen close all and break out of loop
                break;
            end
            %10
            if  recalibrateSubj; %press 'c' to go back to setup screen and recalibrate
                %20
                
                recalibrateSubj;
                %21
                Eyelink('StopRecording');
                
                [windowPtr1, winRect] = Screen('OpenWindow', whichScreen, 127, []); % open simple window for eye track calibration
                el=EyelinkInitDefaults(windowPtr1); % initialize eye tracker default settings
                %22
                EyelinkDoTrackerSetup(el); %hands control off to the eyetracker for calibration
                while keyIsDown % wait for key to be released (otherwise holding down 'esc' may exit out of experiment)
                    [keyIsDown,seconds,keyCode] = KbCheck;
                end
                %23
                Screen('Close', windowPtr1);
                %24
                Eyelink('StartRecording');
                recalibrateSubj = 0;
            end
            
            %             params.driftCorrFrequency = 30;
            %             if (rem(trialCount,params.driftCorrFrequency)) == 0
            %                 Eyelink('StopRecording');
            %                 [windowPtr1, winRect] = Screen('OpenWindow', whichScreen, 127, []); % open simple window for eye track calibration
            %                 el=EyelinkInitDefaults(windowPtr1); % initialize eye tracker default settings
            %                 EyelinkDoDriftCorrection(el);
            %                 Screen('Close', windowPtr1);
            %                 Eyelink('StartRecording');
            %             end
        end
        %11
        if exitExp;
            %3
            break
        end
        experimentDuration=toc(tstart);
        if ResponseType==2
            Results(trialCount, :)= {trialCount, ECC_Index, THETA_Index, eccentricityDeg(ECC_Index), rad2deg(theta(THETA_Index)), acuityOrContrast, (xpix(trialCount)), (ypix(trialCount)), EffectiveStimulusSizePix, pFunc{ECC_Index, THETA_Index,sfIndex}.testLevel, level(nPresented), respCorrect, choice, TestSF, sfIndex,fixCheck, centerX,centerY,eyePos.OSX, eyePos.ODX, eyePos.OSY, eyePos.ODY, eyePos.time, evtSacc,allSaccStore};
            Results2= cell2mat(Results(:,1:(end-8)));
        else
            Results(trialCount, :)= {trialCount, ECC_Index, THETA_Index, eccentricityDeg(ECC_Index), rad2deg(theta(THETA_Index)), acuityOrContrast, (xpix(trialCount)), (ypix(trialCount)), EffectiveStimulusSizePix, pFunc{ECC_Index, THETA_Index,sfIndex}.testLevel, level(nPresented), respCorrect, choice, TestSF, sfIndex,fixCheck,centerX,centerY};
            Results2= cell2mat(Results);
        end
        %13
    end % Big loop for each trial
    
    % save data and clean up
    if eyeTracking; % stop recording
        Eyelink('StopRecording');
        Eyelink('CloseFile');
        etDataFilePath=sprintf('%s.edf', dataFile(1:(end-4))); % write image file path
        Eyelink('ReceiveFile',edfFile, etDataFilePath);
    end
    if eyeTracking; % start recording
        Eyelink('ShutDown');
    end
    
    clear LaplacianOfGaussian LoG %clear large variables before saving, not needed for analysis
    Screen('CloseAll');
    save(dataFile); % save all variables
    
    %% *very* rough analysis for debugging purposes (AK)
    if acuityOrContrast == 1
        threshLevel = .8;pGuess = 1/nAFC;
        pFuncMat = cell2mat(pFunc);% convert pFunc to mat for use with cellfun
        figure;hold all;[fitObjects,~,~,outputThresh] = cellfun(@(x,y,z) fitPFuncnAFC_v2(log10(x(1:(z-1))), y, nAFC, 1,acuityOrContrast,threshLevel),{pFuncMat.level}, {pFuncMat.response},{pFuncMat.nPresented},'un',0); % fit each function using fitpFuncnAFC_v2; use cellfun instead of 'for' loops
        outputThresh = reshape(cell2mat(outputThresh),size(pFuncMat)); % outputThresh stores each threshold
        figure;bar(10.^outputThresh);ylabel('Acuity Threshold (cpd)');xlabel('Eccentricity');
    else % only after contrast thresholds have been measured, plot the whole thing
        threshLevel = .8;
        pFuncMat = cell2mat(pFunc);% convert pFunc to mat for use with cellfun
        [fitObjects,~,~,outputThresh] = cellfun(@(x,y,z) fitPFuncnAFC_v2(log10(x(1:(z-1))), y, nAFC, 1,acuityOrContrast,threshLevel),{pFuncMat.level}, {pFuncMat.response},{pFuncMat.nPresented},'un',0); % fit each function using fitpFuncnAFC_v2; use cellfun instead of 'for' loops
        outputThresh = reshape(cell2mat(outputThresh),size(pFuncMat)); % outputThresh stores each threshold
        
        acuityTmp = (log10(adjustedSF(:,:,end))-sfTestLevels(end)); % figure out what the acuity threshold was
        originalSFWithAcuity = cat(3,log10(adjustedSF),acuityTmp); % add acuity to list of tested SFs
        outputThreshWithAcuity = cat(3,outputThresh,zeros(size(acuityTmp)));% add contrast sensitivity of 1 for the high-frequency cutoff (acuity)
        
        figure;subplot(2,1,1);hold on; %plot standard CSF
        eccCols = lines(length(eccentricityDeg));
        for th = 1:size(outputThreshWithAcuity,2);
            for ecc = 1:length(eccentricityDeg) % one line per eccentricity
                plot((squeeze(originalSFWithAcuity(ecc,th,:))),log10(1./(10.^squeeze(outputThreshWithAcuity(ecc,th,:)))'),'o-','Color', eccCols(ecc,:)); %plot log sensitivity (y-axis) as a function of log spatial frequency (x-axis)
            end
        end
        set(gca,'XTick',-2:1,'XTickLabels',10.^(-2:1));set(gca,'YTick',-1:3,'YTickLabels',10.^(-1:3));legend(arrayfun(@(x) [num2str(x) ' deg'],eccentricityDeg,'un',0)); % set custom x and y tick labels
        xlabel('Spatial frequency'); ylabel('Contrast sensitivity'); %axis labels
        subplot(2,1,2);hold on;set(gca,'ColorOrder',eccCols);
        for th = 1:size(outputThreshWithAcuity,2);
            plot([sfTestLevels 0],log10(1./ 10.^(squeeze(outputThreshWithAcuity(:,th,:)))'),'o-');legend(arrayfun(@(x) [num2str(x) ' deg'],eccentricityDeg,'un',0)); %plot the same thing, now plotting x-axis as log10 units relative to acuity threshold
        end
        xlabel('Log10 units relative to acuity threshold');   ylabel('Log10 Sensitivity');
    end
    
    % todo. To simplify, move this into another file
    %%
catch err
    save(dataFile);

    Screen('CloseAll');
    ShowCursor;
    
    rethrow(err)% something broke, save results so far and clean up

end
