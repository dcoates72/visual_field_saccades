function pFunc = updatePFunc_v2(pFunc, respClass, stepSizeReversal,paramList)
% pFunc = updatePFunc(pFunc, respClass, paramList)
%
% This function works in conjunction with setUpPFunc.m to create and run a
% staircase procedure during an experiment.
% pFunc = the structure PFunc used for this staircase.
% respClass = 1 for correct, 0 for incorrect.
% paramList = an arbitrary list of parameters to store in the struct PFunc.
%
% PJB wrote it. 
% TSAW added comments 16/03/10.

% this version does a 1-up,1-down staircase until the first reversal,
% step size switches to stepSizeReversal after 3 reversals

if pFunc.nReversals == 0
    pFunc.nDown = 1;
else
    pFunc.nDown = 3;
end

if respClass==1 % correct or 'Yes' response
    pFunc.response(pFunc.nPresented)=1;
    pFunc.nYes=pFunc.nYes+1;    % increment yes count
    pFunc.nNo=0;                % zero no count
else % incorrect or No response
    pFunc.response(pFunc.nPresented)=0;
    pFunc.nNo=pFunc.nNo+1;      % increment no count
    pFunc.nYes=0;               % zero yes count
end

if (pFunc.nPresented==pFunc.maxNTrials || pFunc.nReversals==pFunc.maxNTReversals); % if enough trials or reversals reached, terminate condition
    pFunc.condFinished=1;
end

if (pFunc.nReversals==3)    % halve stepsize after 3 reversals
    pFunc.stepSize=stepSizeReversal;
end

for index=1:length(paramList)   % store any trial paramreters passed to the function
    pFunc.trialRecord(index,pFunc.nPresented)=paramList(index);
end

pFunc.lastSign=pFunc.thisSign;
pFunc.nPresented=pFunc.nPresented+1; % increment # trials

if pFunc.nYes==pFunc.nDown; % target # Yes's in a row - lower staircase
    pFunc.level(pFunc.nPresented)=pFunc.level(pFunc.nPresented-1)/pFunc.stepSize; % reduce test level
    pFunc.thisSign=-1; % this staircase going down
    pFunc.nYes=0; % reset # Yes's in a row
elseif pFunc.nNo==pFunc.nUp; % target # No's in a row - increase staircase
    pFunc.level(pFunc.nPresented)=pFunc.level(pFunc.nPresented-1)*pFunc.stepSize; % increase test level
    pFunc.thisSign=1; % this staircase going up
    pFunc.nNo=0; % reset # No's in a row
else % no change
    pFunc.level(pFunc.nPresented)=pFunc.level(pFunc.nPresented-1); % reduce test level
    pFunc.thisSign=pFunc.lastSign; % this staircase stays in same direction
end
if pFunc.thisSign*pFunc.lastSign==-1; % staircase changed direction
   pFunc.nReversals=pFunc.nReversals+1; % increment # reversals
   pFunc.revLevel(pFunc.nReversals)=pFunc.level(pFunc.nPresented); % record level at which observer's response changed
end

pFunc.testLevel=pFunc.level(pFunc.nPresented);