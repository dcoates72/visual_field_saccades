function pFunc = setUpPFunc_v2(maxNTrials, maxNReversals, startGuess, nTrialRecords, nUp, nDown,stepSize)
% pFunc = setUpPFunc(maxNTrials, maxNReversals, startGuess, nTrialRecords, nUp, nDown)
% 
% This function sets up a structure called pFunc that will be used to run
% an up/down staircase. Set up as many pFunc structures (using pFunc(1:N)
% indexing) as needed for conditions in the experiment. This function is
% used in conjunction with updatePFunc to run the staircase. Currently the
% staircase is hard-coded to reduce step size after 3 reversals (see
% updatePFunc.m).
%
% Also see setUpLinearPfunc.
%
% PJB wrote it.
% TSAW added some comments, 16/03/10.

% this version always starts at a specific level (startGuess) rather than choosing
% randomly within +/- 2 steps

pFunc.response=[];     % trial by trial record of observer correct/incorrect responses
pFunc.level=zeros(1,maxNTrials);        % trial by trial record of match level
pFunc.condFinished=0;                % record of when condition finished (either max # trials or desired # staircase reversals
pFunc.stepSize=stepSize;              % staircase starts with large stepsize, then is halved after 1 and 3 reversals
pFunc.nReversals=0;                  % record of # staircase reversals
pFunc.revLevel=zeros(1,maxNReversals);  % record of match level at reversal, used to calculate mean at end of run
pFunc.level(1)=startGuess;% 1st matching level is set by experimenter +/- 0 to 2 steps
pFunc.nPresented=1;                   % count how many trials have been matched for each condition
pFunc.nUp=nUp;                     % count how many trials incorrect before raising staircase.
pFunc.nDown=nDown;                         % count how many trials correct before lowering staircase.
pFunc.nYes=0;                     % count how many trials Yes
pFunc.nNo=0;                         % count how many trials No
pFunc.lastSign=0;                     % memory of level change
pFunc.thisSign=0;                     % new level change
pFunc.testLevel=pFunc.level(pFunc.nPresented);
pFunc.maxNTrials=maxNTrials;
pFunc.maxNTReversals=maxNReversals;
pFunc.nTrialRecords=nTrialRecords;      % store arbitrary number or trial parameters
pFunc.trialRecord=zeros(nTrialRecords,maxNTrials); % trial by trial record
